# 3x8

## Preview

![expresso_light8](expresso_light8.svg)
![expresso_dark8 ](expresso_dark8.svg)

## Colors

| Name        | Hex       | Triple  |
| ----------- | --------- | ------- |
| rosewater   | `#ffdbd2` | ` , , ` |
| flamingo    | `#ffc0c0` | ` , , ` |
| pink        | `#ffb8eb` | ` , , ` |
| mauve       | `#ca9eff` | ` , , ` |
| red         | `#f985a5` | ` , , ` |
| maroon      | `#f497a7` | ` , , ` |
| peach       | `#fcb385` | ` , , ` |
| yellow      | `#fddc95` | ` , , ` |
| green       | `#93f88c` | ` , , ` |
| teal        | `#59f8db` | ` , , ` |
| sky         | `#84eafa` | ` , , ` |
| sapphire    | `#56c6fb` | ` , , ` |
| blue        | `#75aaff` | ` , , ` |
| dark_blue   | `#436597` | ` , , ` |
| bright_blue | `#99c9fb` | ` , , ` |
| lavender    | `#99a4ff` | ` , , ` |
| text        | `#f8f8f8` | ` , , ` |
| subtext     | `#d6d6d6` | ` , , ` |
| overlay     | `#808083` | ` , , ` |
| surface2    | `#68686a` | ` , , ` |
| surface1    | `#505053` | ` , , ` |
| surface0    | `#404043` | ` , , ` |
| underlay    | `#2c2c30` | ` , , ` |
| base        | `#161616` | ` , , ` |
| mantle      | `#191919` | ` , , ` |
| crust       | `#111111` | ` , , ` |

# 3x4

## Preview

![expresso_light4](expresso_light4.svg)
![expresso_dark4 ](expresso_dark4.svg)

## Colors

| Name        | Hex       | Triple  |
| ----------- | --------- | ------- |
| rosewater   | `#ffeedd` | ` , , ` |
| flamingo    | `#ffcccc` | ` , , ` |
| pink        | `#ffbbee` | ` , , ` |
| mauve       | `#cc99ff` | ` , , ` |
| red         | `#ff88aa` | ` , , ` |
| maroon      | `#ff99aa` | ` , , ` |
| peach       | `#ffbb88` | ` , , ` |
| yellow      | `#ffdd99` | ` , , ` |
| green       | `#99ff88` | ` , , ` |
| teal        | `#55ffdd` | ` , , ` |
| sky         | `#88eeff` | ` , , ` |
| sapphire    | `#55ccff` | ` , , ` |
| blue        | `#66aaff` | ` , , ` |
| dark_blue   | `#446699` | ` , , ` |
| bright_blue | `#99ccff` | ` , , ` |
| lavender    | `#99aaff` | ` , , ` |
| text        | `#ffffff` | ` , , ` |
| subtext     | `#dddddd` | ` , , ` |
| overlay     | `#888888` | ` , , ` |
| surface2    | `#666666` | ` , , ` |
| surface1    | `#555555` | ` , , ` |
| surface0    | `#444444` | ` , , ` |
| underlay    | `#333333` | ` , , ` |
| base        | `#333333` | ` , , ` |
| mantle      | `#222222` | ` , , ` |
| crust       | `#111222` | ` , , ` |

