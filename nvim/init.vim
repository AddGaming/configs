" ===============
" general
" ===============

syntax on
set number
set mouse=a
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set smarttab
set smartindent
set termguicolors
set encoding=UTF-8
set foldlevelstart=119
language en_US
set clipboard=unnamedplus
set clipboard+=unnamed
let mapleader = " "
set listchars=eol:↪,tab:⇤–⇥,trail:·,extends:>,precedes:<,space:·
set cursorline 

" ===============
" maps
" ===============

tmap <C-e> <C-\><C-n>
nmap <leader>e :Ex<CR>
nmap <leader>+ 10<C-W>>
nmap <leader>- 10<C-W><
noremap <F5> :set list!<Cr>
imap { {}<ESC>i
imap ( ()<ESC>i
imap [ []<ESC>i
noremap ä @a
noremap <ESC> :noh<CR>
noremap ü :<Up><CR>
noremap ß gg=G``
  

" markdown

autocmd FileType markdown nnoremap <buffer> <C-b> <Esc>bi**<Esc>ea**<Esc>
autocmd FileType markdown inoremap <buffer> <C-b> ****<Esc>hi
autocmd FileType markdown nnoremap <buffer> <C-i> <Esc>bi*<Esc>ea*<Esc>
autocmd FileType markdown inoremap <buffer> <C-i> **<Esc>i
autocmd FileType markdown nnoremap <buffer> <C-m> <Esc>bi`<Esc>ea`<Esc>
" autocmd FileType markdown inoremap <buffer> <C-m> ``<Esc>i
autocmd FileType markdown nnoremap <buffer> <C-l> <Esc>bi[<Esc>ea](<Esc>pa)<Esc>
autocmd FileType markdown inoremap <buffer> <C-l> []()<Esc>hhi
autocmd FileType markdown inoremap " ""<ESC>i
autocmd FileType markdown inoremap ' ''<ESC>i

" html

let g:html_indent_inctags = "p"

autocmd FileType html nnoremap <buffer> <leader>b 0i<b><Cr></b><Esc>k<End>
autocmd FileType html nnoremap <buffer> <leader>i 0i<i><Cr></i><Esc>k<End>
autocmd FileType html nnoremap <buffer> <leader>u 0i<u><Cr></u><Esc>k<End>
autocmd FileType html nnoremap <buffer> <leader>p 0i<p><Cr></p><Esc>k<End>
autocmd FileType html nnoremap <buffer> <leader>d 0i<div style=""><Cr></div><Esc>k<End>
autocmd FileType html nnoremap <buffer> <leader>lu 0i<ul><Cr><li><Cr></li><Cr></ul><Esc>kk<End>
autocmd FileType html nnoremap <buffer> <leader>lo 0i<ol><Cr><li><Cr></li><Cr></ol><Esc>kk<End>

autocmd FileType html xnoremap <leader>a ygvdi<Cr><a href="<Esc>pa"><Cr><Esc>pa<Cr></a><Esc>
autocmd FileType html xnoremap <leader>p ygvdi<p><Esc>pa</p><Esc>
autocmd FileType html xnoremap <leader>h1 ygvdi<h1><Esc>pa</h1><Esc>
autocmd FileType html xnoremap <leader>h2 ygvdi<h2><Esc>pa</h2><Esc>
autocmd FileType html xnoremap <leader>h3 ygvdi<h3><Esc>pa</h3><Esc>
autocmd FileType html xnoremap <leader>h4 ygvdi<h4><Esc>pa</h4><Esc>
autocmd FileType html xnoremap <leader>b y1vdi<b><Esc>pa</b><Esc>
autocmd FileType html xnoremap <leader>i y1vdi<i><Esc>pa</i><Esc>
autocmd FileType html xnoremap <leader>u y1vdi<u><Esc>pa</u><Esc>
autocmd FileType html xnoremap <leader>li y1vdi<li><Cr><Esc>pa<Cr></li><Esc>

" rust
autocmd FileType rust nnoremap <buffer> <leader>dd 0i#[derive(Debug)]<CR><Esc>
autocmd FileType rust inoremap " ""<ESC>i
autocmd FileType rust inoremap ' ''<ESC>i


" ===============
" colors
" ===============

colorscheme catppuccin_expresso
hi link @parameter Parameter
hi link @function Function
hi link @variable Variable
hi link @keyword Keyword
hi link @lsp.type.parameter Parameter
hi link @lsp.type.function Function
hi link @lsp.type.variable Variable

" ===============
" lua
" ===============

lua << EOF
require("config")
EOF
