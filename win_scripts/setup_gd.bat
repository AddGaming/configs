@echo off

:install 
winget install Neovim.Neovim --accept-package-agreements
winget install chrisant996.Clink --accept-package-agreements
winget install Microsoft.PowerShell --accept-package-agreements
winget install Git.Git --accept-package-agreements
winget install Kitware.CMake --accept-package-agreements
:: https://www.cygwin.com/packages/package_list.html
winget install Cygwin.Cygwin --custom "-M"
winget install BurntSushi.ripgrep.MSVC --accept-package-agreements
winget install Obsidian.Obsidian --accept-package-agreements
winget install GodotEngine.GodotEngine.Mono --accept-package-agreements
winget install zig.zig

:configure
mkdir %LOCALAPPDATA%\nvim 
xcopy /S/E/Y %CD%\nvim\ %LOCALAPPDATA%\nvim\
copy /Y terminal\settings.json %LOCALAPPDATA%\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState
copy /Y clink_settings %LOCALAPPDATA%\clink\
mkdir %LOCALAPPDATA%\scripts
xcopy /S/E/Y %CD%\scripts %LOCALAPPDATA%\scripts
start /B /D "C:\Program Files (x86)\clink\" clink.bat autorun install
copy /Y %LOCALAPPDATA%\Microsoft\WinGet\Packages\BurntSushi.ripgrep.MSVC_Microsoft.Winget.Source_8wekyb3d8bbwe\ripgrep-14.1.0-x86_64-pc-windows-msvc\rg.exe %LOCALAPPDATA%\scripts
mklink  %LOCALAPPDATA%\scripts\godot.exe %LOCALAPPDATA%\Microsoft\WinGet\Packages\GodotEngine.GodotEngine.Mono_Microsoft.Winget.Source_8wekyb3d8bbwe\Godot_v4.2.1-stable_mono_win64\Godot_v4.2.1-stable_mono_win64_console.exe

powershell -Command "&{ Start-Process powershell -ArgumentList 'Set-ExecutionPolicy RemoteSigned' -Verb RunAs}"
:: todo: check for existence in path and only then add
powershell -Command "&{ Start-Process powershell -ArgumentList '-File', '%LOCALAPPDATA%\scripts\add2path.ps1', '%LOCALAPPDATA%\scripts' -Verb RunAs}"
powershell -Command "&{ Start-Process powershell -ArgumentList '-File', '%LOCALAPPDATA%\scripts\add2path.ps1', 'C:\OCaml64\bin' -Verb RunAs}"
powershell -Command "&{ Start-Process powershell -ArgumentList '-File', '%LOCALAPPDATA%\scripts\link.ps1', '%LOCALAPPDATA%\scripts\godot.lnk', '%LOCALAPPDATA%\Microsoft\WinGet\Packages\GodotEngine.GodotEngine.Mono_Microsoft.Winget.Source_8wekyb3d8bbwe\Godot_v4.2.1-stable_mono_win64\Godot_v4.2.1-stable_mono_win64_console.exe' -Verb RunAs}"
powershell -Command "&{ Start-Process powershell -ArgumentList '-File', '%LOCALAPPDATA%\scripts\add2pathext.ps1', '.lnk' -Verb RunAs}"

:install_rcedit 
:: for editing windows exe's
set "downloadURL=https://github.com/electron/rcedit/releases/download/v2.0.0/rcedit-x64.exe"
set "outputPath=%CD%\rcedit-x64.exe"
curl -L -o "%outputPath%" "%downloadURL%"
copy /Y "%outputPath%" %LOCALAPPDATA%\scripts

:install_nerdfont
set "fontName=JetBrainsMono"
set "downloadURL=https://github.com/ryanoasis/nerd-fonts/releases/latest/download/%fontName%.zip"
set "outputPath=%CD%\%fontName%.zip"
curl -L -o "%outputPath%" "%downloadURL%"

mkdir %fontName%
tar -xf %fontName%.zip -C %cd%\%fontName%
powershell -Command "&{ Start-Process powershell -ArgumentList '-File', '%CD%\dl_nerdfont.ps1', '%CD%' -Verb RunAs}"
pause
echo continue after installing fonts by pressing [any key]...
rmdir /s /q "%fontName%"
del "%outputPath%"


:launch
nvim setup_c.md 
exit /b 
