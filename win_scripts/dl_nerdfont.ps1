$wd = $args[0]
try {
    # Define variables
    $fontName = "JetBrainsMono"

    $full_path = Join-Path -Path $wd -ChildPath $fontname
    # Install all .ttf files in the directory
    Get-ChildItem -Path $full_path -Filter "*.ttf" | ForEach-Object {
        Write-Host "Installing $($_.FullName)"
            Copy-Item -Path $_.FullName -Destination $destinationPath
    }
}
finally {
    # Keep the window open
    Read-Host -Prompt "Press Enter to exit"
}
