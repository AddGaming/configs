@echo off

:install
winget install Obsidian.Obsidian --accept-package-agreements
winget install Anki.Anki --accept-package-agreements
winget install Git.Git --accept-package-agreements

:configure
mkdir %localappdata%\.obsidian
xcopy /S/E/Y %cd%\.obsidian %localappdata%\.obsidian
mkdir %LOCALAPPDATA%\scripts
xcopy /S/E/Y %CD%\scripts %LOCALAPPDATA%\scripts

powershell -Command "&{ Start-Process powershell -ArgumentList 'Set-ExecutionPolicy RemoteSigned' -Verb RunAs}"
:: todo: check for existence in path and only then add
powershell -Command "&{ Start-Process powershell -ArgumentList '-File', '%LOCALAPPDATA%\scripts\add2path.ps1', '%LOCALAPPDATA%\scripts' -Verb RunAs}"

:launch
exit /b 0
