# Productive Terminal Setup

## How to set up your terminal

Simply execute the `setup.bat` and accept the admin prompts.

***What about Updates?***

Re-run the `setup.bat`

## Whats in the box?

The script does the following:

1. Install Git; the most widely spread version control system.
2. Install Obsidian; a notetaking tool
3. Install Anki; a flash card tool
10. Insert a few small scripts into you path.
   This includes the following commands:
   - [bat] md2html: converts a given markdown file into a html file
   - [ps1] add2path: a script for adding a new entry to the `%PATH%`
   - [bat] obs: obsidian controls

Have fun

