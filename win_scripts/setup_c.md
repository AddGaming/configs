# Productive Terminal Setup

## How to set up your terminal

Simply execute the `setup.bat` and accept the admin prompts.
In the process of installing the Cygwin installer will give you the option
of which packages to install.
Sort the packages by *Category* and install all `Base` and `Devel` packages.

***What about Updates?***

Re-run the `setup.bat`

## Whats in the box?

The script does the following:

1. Install neovim, a powerfull text editor you can use to quickly edit files.
   If everything went ok, the script will end by booting you into neovim.
   Open the editor any time by typing `nvim` inside you console.
   To learn more about vim/neovim; [read the friendly manual](https://neovim.io/doc/user/vim_diff.html#nvim-features)
   Alternatively [listen to the firendly manual](https://youtu.be/rT-fbLFOCy0?si=Xbsy3EkfEM8PrbAN).
   or type `:Tutor` for a more interactive tutoring session.
2. Install Clink, to bring the convinence of the unix shell to the windows shells.
   The full documentation can be found [here](https://chrisant996.github.io/clink/clink.html#clink-settings),
   but even if you don't costumize it further, it should make life easier,
   by offering auto-complete and syntax highlighting.
3. Include basic configuration for neovim and clink.
4. Set a new color scheme for your terminal.
   The color scheme is a variation of the popular catppuccin.
   Feel free to further adjust it by pressing `ctrl + ,` and navigating to `Color schemes`.
   Changes will have imediate effect on both clink and neovim
   (given that you have not installed plugins or used fixed color in the configs).
5. Install PowerShell; An alternative shell from microsoft filled with more useful utils.
6. Install Git; the most widely spread version control system.
7. Install CMake; a tool required for building C like apps using the Visual Studio infrastructure.
8. Install ripgrep; a grep alternative for windows which is also used inside neovim to enable telescopes grep search.
9. Install Cygwin; a collection of tools with many compilers preinstalled.
10. Insert a few small scripts into you path.
   This includes the following commands:
   - [bat] md2html: converts a given markdown file into a html file
   - [bat] nvim: forwards to the neovim exe
   - [bat] cygwin: forwards to the cygwin.bat
   - [bat] gcc: forwards to the gcc.exe
   - [ps1] add2path: a script for adding a new entry to the `%PATH%`

## Whats next?

The setup is meant to get you more familiar and efficient at using the tools of our trait.
Go and take a look at the shortcuts and integrate them into your work flow.
Even if you only integrate them one shortcut at a time, 
you'll be running in a week and flying within a month.

Have fun

## QnA

- Why isn't the package selection automated?
  Because I don't know enough GCC and Clang to handpick the packages and I'm not writing down over 1k packages by hand
  
