@echo off

IF "%1" == "start" goto :boot
If "%1" == "open" goto :load
IF "%1" == "quit" goto :close
IF "%1" == "init" goto :init

echo Unknown parameter. Supported: start, open [vault], quit, init
exit /b 0

:boot
start %localappdata%\Obsidian\Obsidian.exe
exit /b 0

:load 
start "" "obsidian://open?vault=%2"
exit /b 0

:close
taskkill /F /IM Obsidian.exe
exit /b 0

:init
mkdir %cd%\.obsidian
xcopy /S/E/Y %localappdata%\.obsidian %cd%\.obsidian
git init

