@echo off
if "%1" == "" goto :fail

:start
"C:\Program Files\PowerShell\7\pwsh.exe" -Command "(ConvertFrom-Markdown -Path .\%1).Html | Out-File -Encoding utf8 .\%1.html"
exit /b 0

:fail
echo "you need to provide the file name that you want to convert"
exit /b 0
