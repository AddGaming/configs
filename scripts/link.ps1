$from = $args[0]
$to = $args[1]

# Create a WScript.Shell COM object
$WScriptShell = New-Object -ComObject WScript.Shell

# Create the shortcut
$Shortcut = $WScriptShell.CreateShortcut($from)

# Set the target path of the shortcut
$Shortcut.TargetPath = $to

# Save the shortcut
$Shortcut.Save()

